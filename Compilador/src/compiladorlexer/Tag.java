package compiladorlexer;

public class Tag {

    public final static int //palavras reservadas
            START = 256,
            EXIT = 257,
            INT = 258,
            FLOAT = 259,
            STRING = 260,
            IF = 261,
            THEN = 262,
            ELSE = 263,
            END = 264,
            DO = 265,
            WHILE = 266,
            SCAN = 267,
            PRINT = 268,
            NOT = 269,
            OR = 270,
            AND = 271,

            IGUAL_IGUAL = 272,
            MAIOR_IGUAL = 273,
            MENOR_IGUAL = 274,
            MENOR_MAIOR = 275,
            //Identificador
            IDENTIFICADOR = 276,
            //Constantes
            NUMERO = 277,
            LITERAL = 278;
}
