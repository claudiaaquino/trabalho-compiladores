package compiladorlexer;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

public class Lexer {

    public static int line = 1; //contador de linhas do arquivo
    private char ch = ' '; // caractere iterado do arquivo
    private int endFile; // recebe valor -1 para indicar final do arquivo
    private FileReader file;
    private Hashtable tabelaSimbolos = new Hashtable();

    public Lexer(String fileName) throws FileNotFoundException, IllegalArgumentException, IllegalAccessException {
        try {
            file = new FileReader("test/" + fileName);
        } catch (FileNotFoundException e) {
            System.out.println("Arquivo não encontrado");
            throw e;
        }

        //reserva na tabela de símbolos as palavras chaves declaradas na classe Word
        for (Field palavra : Word.class.getFields()) {
            reservar((Word) palavra.get(new Word()));
        }
    }

    //Metodo para inserir palavras reservadas na Tabela de Símbolos
    private void reservar(Word w) {
        tabelaSimbolos.put(w.getLexema(), w);//lexema é a chave de entrada na tabela hash
    }

    //Metodo que busca o próximo caractere do arquivo
    private void readch() throws IOException {
        endFile = file.read();
        if (endFile != -1) {
            ch = (char) endFile;
        }
    }

    //Metodo lê o próximo caractere do arquivo e verifica se é igual a c
    private boolean readch(char c) throws IOException {
        readch();
        if (ch != c) {
            return false;
        }
        ch = ' ';
        return true;
    }

    public Token scan() throws IOException {

        //DEsconsidera delimitadores do arquivo
        ignoraDelimitadores();
        ignoraComentarios();

        if (endFile == -1) {
            return Word.EXIT;//Ao atingir o final do arquivo é retornado o token refenrete ao fim de arquivo
        }

        switch (ch) {
            case ';':
                readch();
                return Word.PONTO_VIRGULA;
            case '=':
                if (readch('=')) {
                    return Word.IGUAL_IGUAL;
                } else {
                    return Word.IGUAL;
                }
            case '(':
                readch();
                return Word.ABRE_PARENTESES;
            case ')':
                readch();
                return Word.FECHA_PARENTESES;
            case '-':
                readch();
                return Word.SUBTRACAO;
            case '>':
                if (readch('=')) {
                    return Word.MAIOR_IGUAL;
                } else {
                    return Word.MAIOR;
                }
            case '<':
                if (readch('=')) {
                    return Word.MENOR_IGUAL;
                } else if (ch == '>') {
                    return Word.MENOR_MAIOR;
                } else {
                    return Word.MENOR;
                }
            case '+':
                readch();
                return Word.SOMA;
            case '*':
                readch();
                return Word.MULTIPLICACAO;
            case '/':
                readch();
                return Word.DIVISAO;
            case ',':
                readch();
                return Word.VIRGULA;
        }

        //Numeros
        if (Character.isDigit(ch)) {
            StringBuilder strBuilder = new StringBuilder();
            do {
                strBuilder.append(ch);
                readch();
            } while (Character.isDigit(ch));

            //Float
            if (ch == '.') {
                strBuilder.append('.');
                readch();
                if (Character.isDigit(ch)) {
                    do {
                        strBuilder.append(ch);
                        readch();
                    } while (Character.isDigit(ch));
                    return new Num(Float.parseFloat(strBuilder.toString()));
                }
            } else {
                return new Num(Integer.parseInt(strBuilder.toString()));
            }
        }

        //Identificadores
        if (Character.isLetter(ch)) {
            StringBuffer sb = new StringBuffer();
            do {
                sb.append(ch);
                readch();
            } while (Character.isLetterOrDigit(ch));

            String s = sb.toString();
            Word w = (Word) tabelaSimbolos.get(s);
            if (w != null) {
                return w;//Retorna palavra salva na TS
            }
            w = new Word(s, Tag.IDENTIFICADOR);
            tabelaSimbolos.put(s, w);
            return w;
        }

        // Literais e Strings
        if (ch == '"') {
            StringBuffer sb = new StringBuffer();
            do {
                sb.append(ch);
                readch();
                if (endFile == -1) {
                    return Word.EXIT;//Ao atingir o final do arquivo é retornado o token refenrete ao fim de arquivo
                }
            } while (ch != '"');
            sb.append(ch);
            readch();

            String s = sb.toString();
            Word w = new Word(s, Tag.LITERAL);
            tabelaSimbolos.put(s, w);
            return w;
        }

        //Caracteres ASCII
        readch();
        return new Token(ch);
    }

    public static int getLine() {
        return line;
    }

    public char getChar() {
        return ch;
    }

    public Hashtable getTabelaSimbolos() {
        return tabelaSimbolos;
    }

    public void ignoraDelimitadores() throws IOException {
        for (;; readch()) {
            if (ch == ' ' || ch == '\t' || ch == '\r' || ch == '\b') {
                continue;
            } else if (ch == '\n') {
                line++;//conta linhas do cóodigo
            } else {
                break;
            }
        }
    }
// salta comentários

    public void ignoraComentarios() throws IOException {

        if (ch == '{') {
            while (ch != '}' && endFile != -1) {
                readch();
            }
            readch();
        }
    }
}
