package compiladorlexer;

public class Num extends Token {

    public int valueINT = 0;
    public float valueFLOAT = 0;

    public Num(int value) {
        super(Tag.INT);
        this.valueINT = value;
    }

    public Num(float value) {
        super(Tag.FLOAT);
        this.valueFLOAT = value;
    }

    public String toString() {
        return "" + (valueINT > 0 ? valueINT : valueFLOAT);
    }

    public int getInt() {
        return valueINT;
    }
    
    public float getFloat() {
        return valueFLOAT;
    }
}
