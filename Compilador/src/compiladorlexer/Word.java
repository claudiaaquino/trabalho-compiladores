package compiladorlexer;

public class Word extends Token {

    private String lexema = "";

    public static final Word START = new Word("start", Tag.START);
    public static final Word EXIT = new Word("exit", Tag.EXIT);
    public static final Word INT = new Word("int", Tag.INT);
    public static final Word FLOAT = new Word("float", Tag.FLOAT);
    public static final Word STRING = new Word("string", Tag.STRING);
    public static final Word IF = new Word("if", Tag.IF);
    public static final Word THEN = new Word("then", Tag.THEN);
    public static final Word ELSE = new Word("else", Tag.ELSE);
    public static final Word DO = new Word("do", Tag.DO);
    public static final Word WHILE = new Word("while", Tag.WHILE);
    public static final Word END = new Word("end", Tag.END);
    public static final Word SCAN = new Word("scan", Tag.SCAN);
    public static final Word PRINT = new Word("print", Tag.PRINT);
    public static final Word AND = new Word("and", Tag.AND);
    public static final Word OR = new Word("or", Tag.OR);
    public static final Word NOT = new Word("not", Tag.NOT);
    public static final Word ABRE_CHAVES = new Word("{", '{');
    public static final Word FECHA_CHAVES = new Word("}", '}');
    public static final Word PONTO_VIRGULA = new Word(";", ';');
    public static final Word ABRE_PARENTESES = new Word("(", '(');
    public static final Word FECHA_PARENTESES = new Word(")", ')');
    public static final Word SUBTRACAO = new Word("-", '-');
    public static final Word IGUAL_IGUAL = new Word("==", Tag.IGUAL_IGUAL);
    public static final Word IGUAL = new Word("=", '=');
    public static final Word MAIOR = new Word(">", '>');
    public static final Word MAIOR_IGUAL = new Word(">=", Tag.MAIOR_IGUAL);
    public static final Word MENOR = new Word("<", '<');
    public static final Word MENOR_IGUAL = new Word("<=", Tag.MENOR_IGUAL);
    public static final Word MENOR_MAIOR = new Word("<>", Tag.MENOR_MAIOR);
    public static final Word SOMA = new Word("+", '+');
    public static final Word MULTIPLICACAO = new Word("*", '*');
    public static final Word DIVISAO = new Word("/", '/');
    public static final Word IDENTIFICADOR = new Word("", Tag.IDENTIFICADOR);
    public static final Word NUMERO = new Word("", Tag.NUMERO);
    public static final Word COMMENT_TEXT_ABRE = new Word("{", '{');
    public static final Word COMMENT_TEXT_FECHA = new Word("}", '}');
    public static final Word VIRGULA = new Word(",", ',');

    public Word() {
        super(0);
    }

    public Word(String s, int tag) {
        super(tag);
        lexema = s;
    }

    public String toString() {
        return "" + lexema;
    }

    public String getLexema() {
        return lexema;
    }

    public void setLexema(String lexema) {
        this.lexema = lexema;
    }
}
