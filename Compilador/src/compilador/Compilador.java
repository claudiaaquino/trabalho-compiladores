package compilador;

import compiladorlexer.Tag;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class Compilador {

    public static void main(String[] args) throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException {
        int numTeste = 1;

        for (; numTeste <= 6; numTeste++) {
            System.out.println("Teste do programa " + numTeste);
            compiladorlexer.Lexer lexico = new compiladorlexer.Lexer("teste" + numTeste + ".txt");
            compiladorlexer.Token t = new compiladorlexer.Token(0);

            FileWriter writer = new FileWriter("test/outputTeste" + numTeste + ".txt"); // escrever a saida em arquivo p facilitar pro relatorio
            writer.write("Tokens lidos no código de entrada: \n");
            System.out.println("Tokens lidos do arquivo: ");

            //Encontro não encontrar o token EXIT ou o final do arquivo
            while (t.getTag() != Tag.EXIT) {
                t = lexico.scan();
                System.out.println(t.toString());
                writer.write(t.toString() + "\n");
            }

            System.out.println("Arquivo lido.");
            writer.write("\n\n\nTabela de Simbolos gerada\n");
            System.out.println("Conteúdo da Tabela de Símbolos : "
                    + "\n" + lexico.getTabelaSimbolos().toString() + "\n\n\n");
            writer.write("\n" + lexico.getTabelaSimbolos().toString() + "\n");
            writer.close();
        }
    }
}
